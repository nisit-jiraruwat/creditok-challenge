module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        primary: {
          light: '#3CC098',
          DEFAULT: '#088c5b',
          dark: '#057b4f'
        }
      }
    },
  },
  plugins: [
    require('@tailwindcss/line-clamp')
  ]
}
