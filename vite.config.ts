import { defineConfig } from 'vite'
import vueJsx from '@vitejs/plugin-vue-jsx'

const isProd = process.env.NODE_ENV === 'production'

// https://vitejs.dev/config/
export default defineConfig({
  base: isProd ? '/creditok-challenge/' : '/',
  resolve: {
    alias: {
      '@/': '/src/'
    }
  },
  plugins: [vueJsx()]
})
