import { defineComponent } from 'vue'
import { RouterView } from 'vue-router'

import Navbar from '@/components/common/Navbar'
import Sidebar from '@/components/common/Sidebar'

const App = defineComponent({
  setup() {
    return () => (
      <>
        <div class='w-full'>
          <Navbar />
          <RouterView />
        </div>
        <Sidebar />
      </>
    );
  },
})

export default App
