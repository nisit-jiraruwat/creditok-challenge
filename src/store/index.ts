import { createStore } from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import searchModule, { State as SearchState } from '@/store/modules/search'
import bookModule, { State as BookState } from '@/store/modules/book'
import favoriteBooksModule, { State as FavoriteBooksState } from '@/store/modules/favorite-books'

export interface RootState {
  search: SearchState
  book: BookState
  favoriteBooks: FavoriteBooksState
}

const store = createStore({
  modules: {
    search: searchModule,
    book: bookModule,
    favoriteBooks: favoriteBooksModule
  },
  plugins: [
    createPersistedState({
      paths: ['favoriteBooks']
    })
  ]
})

export default store
