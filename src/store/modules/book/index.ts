import { Module } from 'vuex'

import { fetchBook } from '@/apis/googleapis'
import { MutationTypes } from '@/store/modules/book/mutation-types'
import { ActionTypes } from '@/store/modules/book/action-types'
import { Book } from '@/types/Book'

export interface State {
  details?: Book
  isFetching: boolean
}

const bookModule: Module<State, any> = {
  state () {
    return {
      details: undefined,
      isFetching: false
    }
  },
  mutations: {
    [MutationTypes.SET_BOOK] (state, payload: { bookDetails: Book }) {
      state.details = payload.bookDetails
    },
    [MutationTypes.SET_FETCHING_BOOK] (state, payload: { value: boolean }) {
      state.isFetching = payload.value
    }
  },
  actions: {
    [ActionTypes.FETCH_BOOK] ({ commit }, playload: { bookId: string }) {
      commit(MutationTypes.SET_FETCHING_BOOK, { value: true })
      fetchBook(playload.bookId)
        .then((book) => {
          commit(MutationTypes.SET_BOOK, { bookDetails: book })
        })
        .finally(() => {
          commit(MutationTypes.SET_FETCHING_BOOK, { value: false })
        })
    }
  }
}

export default bookModule
