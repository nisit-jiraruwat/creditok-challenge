export enum MutationTypes {
  SET_BOOK = 'SET_BOOK',
  SET_FETCHING_BOOK = 'SET_FETCHING_BOOK'
}
