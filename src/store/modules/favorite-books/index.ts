import { Module } from 'vuex'

import { MutationTypes } from '@/store/modules/favorite-books/mutation-types'
import { ActionTypes } from '@/store/modules/favorite-books/action-types'
import { Book } from '@/types/Book'

export interface State {
  books: Book[]
}

const favoriteBooksModule: Module<State, any> = {
  state () {
    return {
      books: []
    }
  },
  mutations: {
    [MutationTypes.ADD_FAVORITE_BOOK] (state, payload: { book: Book }) {
      state.books.push(payload.book)
    },
    [MutationTypes.REMOVE_FAVORITE_BOOK] (state, payload: { book: Book }) {
      const index = state.books.findIndex(({ id }) => id === payload.book.id)

      if (index !== -1) {
        state.books.splice(index, 1)
      }
    }
  },
  actions: {
    [ActionTypes.ADD_FAVORITE_BOOK] ({ commit }, playload: { book: Book }) {
      commit(MutationTypes.ADD_FAVORITE_BOOK, { book: playload.book })
    },
    [ActionTypes.REMOVE_FAVORITE_BOOK] ({ commit }, playload: { book: Book }) {
      commit(MutationTypes.REMOVE_FAVORITE_BOOK, { book: playload.book })
    }
  }
}

export default favoriteBooksModule
