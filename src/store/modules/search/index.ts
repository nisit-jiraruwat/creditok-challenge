import { createStore, Module } from 'vuex'

import { fetchBooks } from '@/apis/googleapis'
import { MutationTypes } from '@/store/modules/search/mutation-types'
import { ActionTypes } from '@/store/modules/search/action-types'
import { Book } from '@/types/Book'

export interface State {
  totalBooks: number
  displayBooks: Book[]
  isSearching: boolean
}

const searchModule: Module<State, any> = {
  state () {
    return {
      totalBooks: 0,
      displayBooks: [],
      isSearching: false
    }
  },
  mutations: {
    [MutationTypes.SET_SEARCH_BOOKS] (state, payload: { totalBooks: number, displayBooks: Book[] }) {
      state.displayBooks = payload.displayBooks
      state.totalBooks = payload.totalBooks
    },
    [MutationTypes.SET_SEARCHING_BOOKS] (state, payload: { value: boolean }) {
      state.isSearching = payload.value
    }
  },
  actions: {
    [ActionTypes.SEARCH_BOOKS] ({ commit }, playload: { params: { q?: string, maxResults?: number } }) {
      commit(MutationTypes.SET_SEARCHING_BOOKS, { value: true })
      fetchBooks(playload.params)
        .then(({ items, totalItems }) => {
          const displayBooks = items === undefined ? [] : items
          commit(MutationTypes.SET_SEARCH_BOOKS, { totalBooks: totalItems, displayBooks })
        })
        .finally(() => {
          commit(MutationTypes.SET_SEARCHING_BOOKS, { value: false })
        })
    },

  }
}

export default searchModule
