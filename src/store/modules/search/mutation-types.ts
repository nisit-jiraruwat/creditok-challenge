export enum MutationTypes {
  SET_SEARCH_BOOKS = 'SET_SEARCH_BOOKS',
  SET_SEARCHING_BOOKS = 'SET_SEARCHING_BOOKS'
}
