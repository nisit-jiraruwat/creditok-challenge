export interface Book {
  id: string
  volumeInfo: {
    title: string
    authors?: string[]
    averageRating: number
    ratingsCount: number
    imageLinks?: {
      smallThumbnail: string
      thumbnail: string
    }
    language: string
    previewLink: string
    infoLink: string
    canonicalVolumeLink: string
    publishedDate: string
    description?: string
    industryIdentifiers?: Array<{
      type: string
      identifier: string
    }>
    pageCount: number
  }
  saleInfo: {
    listPrice?: {
      amount: number
    }
  }
}
