import { createApp } from 'vue'

import '@/styles/globals.css'
import App from '@/App'
import router from '@/router'
import store from '@/store'

createApp(App)
  .use(router)
  .use(store)
  .mount('#app')
