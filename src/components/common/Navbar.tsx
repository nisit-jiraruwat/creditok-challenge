import { defineComponent, ref, watch } from 'vue'
import { useRoute, useRouter } from 'vue-router'

const Navbar = defineComponent({
  setup () {
    const route = useRoute()
    const router = useRouter()
    const search = ref<string>('')

    const handleSearchBooks = (e: Event) => {
      e.preventDefault()
      const q = search.value.trim()

      if (q === '') {
        return
      }

      router.push({ name: 'search', query: { q } })
    }

    watch(
      () => route.query,
      () => {
        search.value = route.query.q !== undefined ? route.query.q as string : ''
      },
      { immediate: true }
    )

    return () => (
      <div class='flex justify-center items-center pr-10 py-4 bg-white'>
        <form
          class='w-1/2 flex'
          onSubmit={handleSearchBooks}
        >
          <input
            v-model={search.value}
            class='w-full border-2 border-primary'
            type='text'
            placeholder='Title Keyword'
          />
          <button
            class='btn w-10 h-10 flex justify-center !rounded-none text-white bg-primary'
            type='submit'
          >
            <svg
              xmlns='http://www.w3.org/2000/svg'
              class='h-6 w-6'
              fill='none'
              viewBox='0 0 24 24'
              stroke='currentColor'
            >
              <path stroke-linecap='round' stroke-linejoin='round' stroke-width={2} d='M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z' />
            </svg>
          </button>
        </form>
      </div>
    );
  },
})

export default Navbar
