import { computed, defineComponent, PropType } from 'vue'
import { useStore } from 'vuex'

import BookInfo from '@/components/search/BookInfo'
import SortBy from '@/components/search/SortBy'
import { RootState }  from '@/store'

const SearchResults = defineComponent({
  props: {
    title: {
      type: String as PropType<string>,
      required: true
    },
    isShowToolbar: {
      type: Boolean as PropType<boolean>,
      default: true
    }
  },
  setup (props) {
    const store = useStore<RootState>()
    const searchState = computed(() => store.state.search)

    return () => {
      if (searchState.value.isSearching) {
        return (
          <div class='w-full flex justify-center items-center pt-40'>
            <div class='animate-spin'>
              <svg class='h-12 w-12' xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 24 24'>
                <circle class='opacity-25' cx='12' cy='12' r='10' stroke='currentColor' stroke-width='3'></circle>
                <path class='opacity-75' fill='currentColor' d='M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z'></path>
              </svg>
            </div>
          </div>
        )
      }

      return (
        <div class='w-full bg-white'>
          <div class='p-4 bg-primary'>
            <h1 class='text-xl font-bold text-white'>{props.title}</h1>
          </div>
          {props.isShowToolbar &&
            <div class='flex px-4 py-3 border-b border-zinc-200 bg-white'>
              <div class='flex-1'>
                <p class='text-xs font-medium text-gray-600'>{searchState.value.totalBooks} ITEM(S)</p>
              </div>
              <div>
                <SortBy />
              </div>
            </div>}
          <div class='grid grid-cols-12 gap-6 p-3'>
            {searchState.value.displayBooks.map((book) => {
              return (
                <BookInfo
                  key={book.id}
                  class='col-span-3 flex flex-col'
                  book={book}
                />
              )
            })}
          </div>
          {props.isShowToolbar &&
            <div class='flex px-4 py-3 border-t border-zinc-200 bg-white'>
              <div class='flex-1'>
                <p class='text-xs font-medium text-gray-600'>{searchState.value.totalBooks} ITEM(S)</p>
              </div>
              <div>
                <SortBy />
              </div>
            </div>}
        </div>)
    }
  }
})

export default SearchResults
