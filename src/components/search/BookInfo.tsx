import { defineComponent, PropType, computed } from 'vue'
import { RouterLink } from 'vue-router'
import { useStore } from 'vuex'

import { RootState } from '@/store'
import { ActionTypes as FavoriteBooksActionTypes } from '@/store/modules/favorite-books/action-types'
import { Book } from '@/types/Book'

const BookInfo = defineComponent({
  props: {
    book: {
      type: Object as PropType<Book>,
      required: true
    },
    class: {
      type: String as PropType<string>,
      default: ''
    }
  },
  setup (prop) {
    const store = useStore<RootState>()
    const favoriteBooksState = computed(() => store.state.favoriteBooks)

    const thumbnail = computed(() => prop.book.volumeInfo.imageLinks?.smallThumbnail || '/assets/no-image.jpg')
    const authors = computed(() => {
      if (prop.book.volumeInfo.authors === undefined) {
        return undefined
      }

      return `By ${prop.book.volumeInfo.authors.join(', ')}`
    })
    const price = computed(() => {
      if (prop.book.saleInfo.listPrice === undefined) {
        return undefined
      }

      return `฿${prop.book.saleInfo.listPrice.amount}`
    })
    const isFavorite = computed(() => favoriteBooksState.value.books.findIndex(({ id }) => id === prop.book.id) > -1)

    const handleMarkAsFavorite = (e: Event) => {
      e.preventDefault()

      const actionType = isFavorite.value
        ? FavoriteBooksActionTypes.REMOVE_FAVORITE_BOOK
        : FavoriteBooksActionTypes.ADD_FAVORITE_BOOK
      store.dispatch(actionType, { book: prop.book })
    }

    return () => (
      <RouterLink
        class={['group', prop.class]}
        to={{ name: 'book', params: { book_id: prop.book.id } }}
      >
        <div class='flex-1 relative flex items-end p-2 border-b border-zinc-200'>
          <img
            class='w-full'
            src={thumbnail.value}
          />
          <button
            class='btn absolute right-3 top-3 text-white bg-black bg-opacity-60 !p-1'
            type='button'
            onClick={handleMarkAsFavorite}
          >
            <svg
              xmlns='http://www.w3.org/2000/svg'
              class={[
                'h-6 w-6 hover:fill-red-500 hover:text-red-500',
                isFavorite.value && 'fill-red-500 text-red-500'
              ]}
              fill='none'
              viewBox='0 0 24 24'
              stroke='currentColor'
            >
              <path stroke-linecap='round' stroke-linejoin='round' stroke-width={2} d='M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z'
            />
            </svg>
          </button>
        </div>
        <h2
          class='h-12 mt-3 line-clamp-2 hover:text-primary'
          title={prop.book.volumeInfo.title}
        >
          {prop.book.volumeInfo.title}
        </h2>
        <div
          class='h-5 text-sm text-primary-light truncate'
          title={authors.value}
        >
          {authors.value}
        </div>
        <div class='h-7 text-lg font-bold'>
          {price.value}
        </div>
        <div class='h-9'>
          <a
            class='btn w-full flex justify-center text-sm text-white bg-primary transform opacity-0 duration-700 group-hover:opacity-100'
            href={prop.book.volumeInfo.infoLink}
            target='_blank'
            onClick={(e) => {
              e.stopPropagation()
            }}
          >
            BUY
          </a>
        </div>
      </RouterLink>
    )
  }
})

export default BookInfo
