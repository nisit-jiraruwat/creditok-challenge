import { defineComponent, ref, watch } from 'vue'
import { useRoute, useRouter } from 'vue-router'

const SortBy = defineComponent({
  setup () {
    const route = useRoute()
    const router = useRouter()

    const orderBy = ref<string | undefined>()

    const handleSearchBooks = () => {
      const query = {
        ...route.query,
        orderBy: orderBy.value
      }

      router.push({ name: 'search', query })
    }

    watch(
      () => route.query,
      () => {
        orderBy.value = route.query.orderBy ? route.query.orderBy as string : undefined
      },
      { immediate: true }
    )

    return () => (
      <div class='flex'>
        <label class='text-xs text-gray-600'>SORT BY</label>
        <select
          v-model={orderBy.value}
          class='select !py-0 !border-0 text-xs text-primary'
          onChange={handleSearchBooks}
        >
          <option value={undefined}>RELEVANCE</option>
          <option value='newest'>NEWEST</option>
        </select>
      </div>
    )
  }
})

export default SortBy
