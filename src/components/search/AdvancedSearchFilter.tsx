import { defineComponent, ref, watch } from 'vue'
import { useRoute, useRouter } from 'vue-router'

const AdvancedSearchFilter = defineComponent({
  setup () {
    const route = useRoute()
    const router = useRouter()

    const search = ref<string>('')
    const filter = ref<string | undefined>()
    const printType = ref<string | undefined>()
    const language = ref<string | undefined>()

    const handleSearchBooks = (e: Event) => {
      e.preventDefault()
      const q = search.value.trim()

      if (q === '') {
        return
      }

      const query = {
        q,
        filter: filter.value,
        lang: language.value,
        printType: printType.value
      }

      router.push({ name: 'search', query })
    }

    watch(
      () => route.query,
      () => {
        search.value = route.query.q !== undefined ? route.query.q as string : ''
        filter.value = route.query.filter ? route.query.filter as string : undefined
        language.value = route.query.lang ? route.query.lang as string : undefined
        printType.value = route.query.printType ? route.query.printType as string : undefined
      },
      { immediate: true }
    )

    return () => (
      <div class='w-full bg-white'>
        <div class='p-4 border-b border-zinc-200'>
          <h1 class='font-bold'>ADVANCED SEARCH</h1>
        </div>
        <form class='p-3'
          onSubmit={handleSearchBooks}
        >
          <div class='space-y-3'>
            <div>
              <label class='text-xs'>Title Keyword</label>
              <input
                v-model={search.value}
                class='input w-full mt-0.5'
                type='text'
              />
            </div>
            <div>
              <label class='text-xs'>Filter</label>
              <select
                v-model={filter.value}
                class='select w-full mt-0.5'
              >
                <option value={undefined}>-- None --</option>
                <option value='partial'>Partial</option>
                <option value='full'>Full</option>
                <option value='free-ebooks'>Free-ebooks</option>
                <option value='paid-ebooks'>Paid-ebooks</option>
                <option value='ebooks'>Ebooks</option>
              </select>
            </div>
            <div>
              <label class='text-xs'>Print Type</label>
              <select
                v-model={printType.value}
                class='select w-full mt-0.5'
              >
                <option value={undefined}>-- Any Print Type --</option>
                <option value='books'>Books</option>
                <option value='magazines'>Magazines</option>
              </select>
            </div>
            <div>
              <label class='text-xs'>Language</label>
              <select
                v-model={language.value}
                class='select w-full mt-0.5'
              >
                <option value={undefined}>-- Any Language --</option>
                <option value='th'>Thai</option>
                <option value='en'>English</option>
              </select>
            </div>
          </div>
          <div class='mt-3 pt-3 border-t border-zinc-200'>
            <button
              class='btn w-full text-sm text-white bg-primary'
              type='submit'
            >
              SEARCH
            </button>
          </div>
        </form>
      </div>
    )
  }
})

export default AdvancedSearchFilter
