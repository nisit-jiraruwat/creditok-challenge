import { defineComponent, watch } from 'vue'
import { useRoute } from 'vue-router'
import { useStore } from 'vuex'

import AdvancedSearchFilter from '@/components/search/AdvancedSearchFilter'
import SearchResults from '@/components/search/SearchResults'
import { RootState }  from '@/store'
import { ActionTypes as SearchActionTypes } from '@/store/modules/search/action-types'

const Search = defineComponent({
  setup () {
    const store = useStore<RootState>()
    const route = useRoute()

    const searchBooks = () => {
      const params = {
        q: `intitle:${route.query.q}`,
        filter: route.query.filter,
        langRestrict: route.query.lang,
        printType: route.query.printType,
        orderBy: route.query.orderBy,
        maxResults: 40
      }
      store.dispatch(SearchActionTypes.SEARCH_BOOKS, { params })
    }

    watch(
      () => route.query,
      () => {
        searchBooks()
      },
      { immediate: true }
    )

    return () => (
      <div class='flex gap-x-4 pl-4 pr-14 py-6'>
        <div class='w-3/12'>
          <AdvancedSearchFilter />
        </div>
        <div class='w-9/12'>
          <SearchResults title='Search Results' />
        </div>
      </div>
    );
  },
})

export default Search
