import { computed, defineComponent, ref } from 'vue'
import { useStore } from 'vuex'

import SearchResults from '@/components/search/SearchResults'
import { RootState }  from '@/store'
import { ActionTypes as SearchActionTypes } from '@/store/modules/search/action-types'

const BOOK_CATEGORIES = [
  'Action and Adventure',
  'Classics',
  'Comic Book or Graphic Novel',
  'Detective and Mystery',
  'Fantasy',
  'Historical Fiction',
  'Horror',
  'Literary Fiction',
  'Romance',
  'Science Fiction',
  'Short Stories',
  'Suspense and Thrillers',
  'Biographies and Autobiographies',
  'Cookbooks',
  'Essays',
  'History',
  'Memoir',
  'Poetry',
  'Self-Help',
  'True Crime',
]

const Home = defineComponent({
  setup () {
    const store = useStore<RootState>()

    const randomSearchBooks = () => {
      // Random book categories
      const category = BOOK_CATEGORIES[Math.floor(Math.random() * BOOK_CATEGORIES.length)]
      const params = {
        q: `subject:${category}`,
        maxResults: 40
      }
      store.dispatch(SearchActionTypes.SEARCH_BOOKS, { params })
    }
    randomSearchBooks()

    return () => (
      <div class='flex pl-4 pr-14 py-6'>
        <div class='w-9/12 mx-auto'>
          <SearchResults
            title='Books'
            isShowToolbar={false}
          />
        </div>
      </div>
    );
  },
})

export default Home
