import { computed, defineComponent, ref, watch } from 'vue'
import { useStore } from 'vuex'

import BookInfo from '@/components/search/BookInfo'
import { RootState }  from '@/store'

const FavoriteBooks = defineComponent({
  setup () {
    const store = useStore<RootState>()
    const favoriteBooksState = computed(() => store.state.favoriteBooks)

    return () => (
      <div class='pl-4 pr-14 py-6'>
        <div class='w-9/12 mx-auto'>
          <div class='p-4 bg-primary'>
            <h1 class='text-xl font-bold text-white'>My Favorite Books</h1>
          </div>
          <div class='grid grid-cols-12 gap-x-6 gap-y-7 p-3 bg-white'>
            {favoriteBooksState.value.books.map((book) => {
              return (
                <BookInfo
                  key={book.id}
                  class='col-span-3 flex flex-col'
                  book={book}
                />
              )
            })}
          </div>
        </div>
      </div>
    )
  }
})

export default FavoriteBooks
