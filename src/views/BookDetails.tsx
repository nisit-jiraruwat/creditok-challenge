import { computed, defineComponent } from 'vue'
import { useRoute } from 'vue-router'
import { useStore } from 'vuex'

import { RootState }  from '@/store'
import { ActionTypes as BookActionTypes } from '@/store/modules/book/action-types'
import { ActionTypes as FavoriteBooksActionTypes } from '@/store/modules/favorite-books/action-types'

const Home = defineComponent({
  setup () {
    const route = useRoute()
    const store = useStore<RootState>()
    const bookState = computed(() => store.state.book)
    const favoriteBooksState = computed(() => store.state.favoriteBooks)

    const thumbnail = computed(() => bookState.value.details?.volumeInfo.imageLinks?.thumbnail || '/assets/no-image.jpg')
    const authors = computed(() => bookState.value.details?.volumeInfo.authors?.join(', '))

    const price = computed(() => {
      if (bookState.value.details?.saleInfo.listPrice === undefined) {
        return 'Free'
      }

      return `฿${bookState.value.details.saleInfo.listPrice.amount}`
    })

    const industryIdentifiers = computed(() => bookState.value.details?.volumeInfo.industryIdentifiers?.map(({ identifier }) => identifier).join(', '))
    const isFavorite = computed(() => favoriteBooksState.value.books.findIndex(({ id }) => id === route.params.book_id) > -1)

    const handleMarkAsFavorite = () => {
      const actionType = isFavorite.value
        ? FavoriteBooksActionTypes.REMOVE_FAVORITE_BOOK
        : FavoriteBooksActionTypes.ADD_FAVORITE_BOOK
      store.dispatch(actionType, { book: bookState.value.details })
    }

    store.dispatch(BookActionTypes.FETCH_BOOK, { bookId: route.params.book_id })

    return () => {
      if (bookState.value.isFetching) {
        return (
          <div class='w-full flex justify-center items-center pt-40'>
            <div class='animate-spin'>
              <svg class='h-12 w-12' xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 24 24'>
                <circle class='opacity-25' cx='12' cy='12' r='10' stroke='currentColor' stroke-width='3'></circle>
                <path class='opacity-75' fill='currentColor' d='M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z'></path>
              </svg>
            </div>
          </div>
        )
      }

      return (
        <div class='pl-4 pr-14 py-6'>
          <div class='flex'>
            <div class='w-4/12 p-14 bg-zinc-200'>
              <img
                class='w-full'
                src={thumbnail.value}
              />
            </div>
            <div class='w-8/12 p-5 bg-white'>
              <div class='pb-3 border-b border-zinc-200'>
                <h1 class='text-4xl font-bold'>{bookState.value.details?.volumeInfo.title}</h1>
                <div class='mt-3 text-sm text-gray-400'>
                  {authors.value && <span>By {authors.value}</span>}
                </div>
              </div>
              <div class='pb-3 mt-3 border-b border-zinc-200'>
                <div class='p-3 border bg-white'>
                  <span class='text-sm'>Price </span><span class='text-4xl font-bold'>{price.value}</span>
                </div>
                <div class='flex gap-x-3 mt-3'>
                  <a
                    class='btn flex-1 flex justify-center text-sm text-white bg-primary'
                    href={bookState.value.details?.volumeInfo.infoLink}
                    target='_blank'
                  >
                    BUY
                  </a>
                  <button
                    class='group btn flex-1 flex justify-center items-center space-x-2 text-sm'
                    type='button'
                    onClick={handleMarkAsFavorite}
                  >
                    <svg
                      xmlns='http://www.w3.org/2000/svg'
                      class={[
                        'h-6 w-6 group-hover:fill-red-500 group-hover:text-red-500',
                        isFavorite.value && 'fill-red-500 text-red-500'
                      ]}
                      fill='none'
                      viewBox='0 0 24 24'
                      stroke='currentColor'
                    >
                      <path stroke-linecap='round' stroke-linejoin='round' stroke-width={1} d='M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z'
                    />
                    </svg>
                    {!isFavorite.value && <span class='group-hover:text-primary'>MARK AS FAVORITE</span>}
                  </button>
                </div>
              </div>
              <div class='mt-3'>
                <h1 class='text-xl font-bold text-primary'>DESCRIPTION</h1>
                <p
                  class='mt-3 text-sm text-gray-400'
                  v-html={bookState.value.details?.volumeInfo.description}
                >
                  
                </p>
              </div>
            </div>
          </div>
          <div class='p-7 mt-3 bg-white'>
            <h1 class='text-xl font-bold text-primary'>PRODUCT DETAILS</h1>
            <table class='table-fixed mt-3 border-collapse border border-zinc-200'>
              <tbody>
                <tr>
                  <td class='p-2.5 text-sm text-gray-500 border border-zinc-200 bg-zinc-100'>Industry Identifiers</td>
                  <td class='p-2.5 text-sm border border-gray-200'>{industryIdentifiers.value}</td>
                </tr>
                <tr>
                  <td class='p-2.5 text-sm text-gray-500 border border-zinc-200 bg-zinc-100'>Author</td>
                  <td class='p-2.5 text-sm border border-gray-200'>{authors.value}</td>
                </tr>
                <tr>
                  <td class='p-2.5 text-sm text-gray-500 border border-zinc-200 bg-zinc-100'>Published Date</td>
                  <td class='p-2.5 text-sm border border-gray-200'>{bookState.value.details?.volumeInfo.publishedDate}</td>
                </tr>
                <tr>
                  <td class='p-2.5 text-sm text-gray-500 border border-zinc-200 bg-zinc-100'>Number of Pages</td>
                  <td class='p-2.5 text-sm border border-gray-200'>{bookState.value.details?.volumeInfo.pageCount}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      )
    }
  },
})

export default Home
