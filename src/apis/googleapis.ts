import axios from 'axios'

import { Book } from '@/types/Book'

export const fetchBooks = async (params: { q?: string }): Promise<{totalItems: number, items?: Book[]}> => {
  return axios.get(
    'https://www.googleapis.com/books/v1/volumes',
    {
      params
    })
    .then(({ data }: { data: { totalItems: number, items?: Book[] }}) => {
      return data
    })
}

export const fetchBook = async (bookId: string): Promise<Book> => {
  return axios.get(
    `https://www.googleapis.com/books/v1/volumes/${bookId}`)
    .then(({ data }: { data: Book}) => {
      return data
    })
}
