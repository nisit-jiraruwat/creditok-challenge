import { createRouter, createWebHistory } from 'vue-router'

import Home from '@/views/Home'
import Search from '@/views/Search'
import BookDetails from '@/views/BookDetails'
import FavoriteBooks from '@/views/FavoriteBooks'
FavoriteBooks

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/search',
    name: 'search',
    component: Search
  },
  {
    path: '/book/:book_id',
    name: 'book',
    component: BookDetails
  },
  {
    path: '/favorite-books',
    name: 'favorite-books',
    component: FavoriteBooks
  }
]

const router = createRouter({
  history: createWebHistory(import.meta.env.PROD ? '/creditok-challenge/' : '/'),
  routes
})

export default router
